# HDxPersistence

[![CI Status](https://img.shields.io/travis/Milos Babic/HDxPersistence.svg?style=flat)](https://travis-ci.org/Milos Babic/HDxPersistence)
[![Version](https://img.shields.io/cocoapods/v/HDxPersistence.svg?style=flat)](https://cocoapods.org/pods/HDxPersistence)
[![License](https://img.shields.io/cocoapods/l/HDxPersistence.svg?style=flat)](https://cocoapods.org/pods/HDxPersistence)
[![Platform](https://img.shields.io/cocoapods/p/HDxPersistence.svg?style=flat)](https://cocoapods.org/pods/HDxPersistence)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

HDxPersistence is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'HDxPersistence'
```

## Author

Milos Babic, milos.babic@symphony.is

## License

HDxPersistence is available under the MIT license. See the LICENSE file for more info.
