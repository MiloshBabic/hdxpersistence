//
//  ContextManager.swift
//  Persistence
//
//  Created by Milos Babic on 6/25/18.
//  Copyright © 2018 Milos Babic. All rights reserved.
//

import Foundation
import CoreData

class ContextManager {
    static let instance: ContextManager = ContextManager()
    
    let queue = DispatchQueue(label: "ContextQueue", attributes: .concurrent)
    
    var contexts: ThreadSafeQueue<NSManagedObjectContext> = ThreadSafeQueue()
    
    var completes: [NSManagedObjectContext: (dataModify: () -> Void, complete: () -> Void)] = [:]
    
    private init() {
        NotificationCenter.default.addObserver(self, selector: #selector(contextDidSave), name: NSNotification.Name.NSManagedObjectContextDidSave, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func contextDidSave() {
        if let firstInQueue = contexts.last {
            firstInQueue.refreshAllObjects()
            completes[firstInQueue]?.dataModify()
            
            do {
                contexts.removeLast()
                if firstInQueue.hasChanges {
                    try firstInQueue.save()
                    completes[firstInQueue]?.complete()
                }
                
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    func push(context: NSManagedObjectContext, dataModify: @escaping () -> Void, complete: @escaping () -> Void) {

        let contextsWasEmpty = contexts.isEmpty
            
        contexts.addFirst(context)
        completes[context] = (dataModify, complete)
        
        if contextsWasEmpty {
            contextDidSave()
        }
    }
}

class ThreadSafeQueue<T> {
    private var queue: [T] = []
    private let accessQueue = DispatchQueue(label: "ThreadSafeQueue", attributes: .concurrent)
    
    var last: T? {
        return accessQueue.sync { return queue.last }
    }
    
    var isEmpty: Bool {
        return accessQueue.sync { queue.isEmpty }
    }
    
    func addFirst(_ element: T) {
        accessQueue.async(flags: .barrier) { [weak self] in
            self?.queue.insert(element, at: 0)
        }
    }

    func removeLast() {
        accessQueue.async(flags: .barrier) { [weak self] in
            _ = self?.queue.popLast()
        }
    }
}
