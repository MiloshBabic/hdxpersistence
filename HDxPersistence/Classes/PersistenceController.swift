//
//  PersistenceController.swift
//  Persistence
//
//  Created by Milos Babic on 6/20/18.
//  Copyright © 2018 Milos Babic. All rights reserved.
//

import Foundation
import CoreData

public class PersistenceController {
    
    public static let instance = PersistenceController()
    
    public var container: NSPersistentContainer!
    
    public func initialize(modelName: String) {
        container = NSPersistentContainer(name: modelName, bundle: Bundle(for: PersistenceController.self))
    
        container.loadPersistentStores { (storeDescription, error) in
            if let error = error {
                fatalError("Failed to load store \(error.localizedDescription)")
            }
        }
        
        container.viewContext.automaticallyMergesChangesFromParent = true
    }
}

extension NSPersistentContainer {
    convenience init(name: String, bundle: Bundle) {
        
        guard let modelURL = bundle.url(forResource: name, withExtension: "momd"),
            let mom = NSManagedObjectModel.init(contentsOf: modelURL) else {
                
                self.init(name: name)
                
                return
        }
        
        self.init(name: name, managedObjectModel: mom)
    }
}
