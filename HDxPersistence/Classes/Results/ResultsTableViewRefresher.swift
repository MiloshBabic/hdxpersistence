//
//  ResultsRefresher.swift
//  Persistence
//
//  Created by Milos Babic on 6/26/18.
//  Copyright © 2018 Milos Babic. All rights reserved.
//

import Foundation
import CoreData

public protocol ResultsRefreshable: NSFetchedResultsControllerDelegate {}

public class ResultsTableViewRefresher: NSObject, ResultsRefreshable {
    
    let tableView: UITableView
    
    public init(tableView: UITableView) {
        self.tableView = tableView
    }
    
    public func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .update:
            if let indexPath = indexPath {
                tableView.reloadRows(at: [indexPath], with: .automatic)
            }
        case .insert:
            if let newIndexPath = newIndexPath {
                tableView.insertRows(at: [newIndexPath], with: .automatic)
            }
        case .delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .automatic)
            }
        case .move:
            if let indexPath = indexPath, let newIndexPath = newIndexPath {
                if indexPath == newIndexPath {
                    tableView.reloadRows(at: [newIndexPath], with: .automatic)
                } else {
                    tableView.deleteRows(at: [indexPath], with: .automatic)
                }
            }
        }
    }
    
    public func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    public func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
}

public class ResultsDefaultRefresher<T: Base>: NSObject, ResultsRefreshable, NSFetchedResultsControllerDelegate {
    
    public struct Change {
        public var type: NSFetchedResultsChangeType
        public var indexPath: IndexPath?
        public var newIndexPath: IndexPath?
        public var object: T
    }

    var accumulatedChanges: (([Change]) -> Void)?
    
    var changes: [Change] = []
    
    var deletes: [Change] = []
    var inserts: [Change] = []
    var refreshes: [Change] = []
    var moves: [Change] = []
    
    
    var sortedChanges: [Change] {
        self.deletes.sort {
            if let leftIndexPath = $0.indexPath, let rightIndexPath = $1.indexPath {
                return leftIndexPath < rightIndexPath
            }
            return false
        }
        
        self.inserts.sort {
            if let leftIndexPath = $0.newIndexPath, let rightIndexPath = $1.newIndexPath {
                return leftIndexPath < rightIndexPath
            }
            return false
        }
        
        
        return deletes + inserts + moves + refreshes
    }
    
    public init(accumulatedChanges: @escaping ([Change]) -> Void) {
        self.accumulatedChanges = accumulatedChanges
    }
    
    public func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        changes = []
    }
    
    public func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        accumulatedChanges?(sortedChanges)
    }
    
    public func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        if let object = anObject as? T {
            let change = Change(type: type, indexPath: indexPath, newIndexPath: newIndexPath, object: object)
            switch type {
            case .delete: deletes.append(change)
            case .insert: inserts.append(change)
            case .update: refreshes.append(change)
            case .move: moves.append(change)
            }
            
            changes.append(change)
        }
    }
}
