//
//  Results.swift
//  Persistence
//
//  Created by Milos Babic on 6/26/18.
//  Copyright © 2018 Milos Babic. All rights reserved.
//

import Foundation
import CoreData

public struct Results<T: Base & NSManagedObject> {
    public var fetchedResultsController: NSFetchedResultsController<T>
    
    private var delegate: ResultsRefreshable?
    
    public init(predicate: NSPredicate? = nil,
         sortBy: [NSSortDescriptor]? = nil,
         groupBy: [String]? = nil,
         context: NSManagedObjectContext = PersistenceController.instance.container.viewContext,
         refresher: ResultsRefreshable? = nil) {
        
        let fetchRequest = NSFetchRequest<T>(entityName: "\(T.self)")
        fetchRequest.predicate = predicate
        fetchRequest.propertiesToGroupBy = groupBy
        fetchRequest.sortDescriptors = sortBy ?? [NSSortDescriptor(key: "id", ascending: true)]
        
        delegate = refresher        
        fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                              managedObjectContext: context,
                                                              sectionNameKeyPath: nil,
                                                              cacheName: nil)
        do {
            fetchedResultsController.delegate = delegate
            try fetchedResultsController.performFetch()

        } catch {
            print("\(error)")
        }
    }
    
    public func object(at indexPath: IndexPath) -> T {
        return fetchedResultsController.object(at: indexPath)
    }
    
    public var numberOfSections: Int {
        return fetchedResultsController.sections?.count ?? 0
    }
    
    public func numberOfObjects(in section: Int) -> Int {
        if let sections = fetchedResultsController.sections {
            return sections[section].numberOfObjects
        }
        return 0
    }
    
    public var allObjects: [T] {
        for index in 0..<numberOfSections {
            return objects(for: index).compactMap { $0 }
        }
        return []
    }
    
    public func objects(for section: Int) -> [T] {
        if let sections = fetchedResultsController.sections, let baseObjects = sections[section].objects as? [T] {
            return baseObjects
        }
        return []
    }
}
