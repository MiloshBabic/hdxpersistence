//
//  Base.swift
//  Persistence
//
//  Created by Milos Babic on 6/20/18.
//  Copyright © 2018 Milos Babic. All rights reserved.
//

import Foundation
import CoreData
import ObjectMapper

public typealias JSONObject = [String: Any]

public protocol Base {
    associatedtype EntityID: CVarArg
    
    static var jsonKey: String { get }
    
    static func getAll(sourceContext: NSManagedObjectContext) -> [Self]
    static func get(id: EntityID, sourceContext: NSManagedObjectContext, shouldCreate: Bool) -> Self?
    static func get(predicate: NSPredicate,
                    sortDescriptors: [NSSortDescriptor]?,
                    sourceContext: NSManagedObjectContext) -> [Self]

    func delete(from context: NSManagedObjectContext)
    static func delete(with predicate: NSPredicate, from context: NSManagedObjectContext)
}

extension Base where Self: NSManagedObject {

    public static var jsonKey: String { return "id" }
    
    public static func get(id: EntityID, sourceContext: NSManagedObjectContext = PersistenceController.instance.container.viewContext, shouldCreate: Bool = false) -> Self? {
 
        let fetchRequest = NSFetchRequest<Self>(entityName: "\(Self.self)")
        fetchRequest.predicate = NSPredicate(format: "id = %@", id)

        do {
            if let result = try sourceContext.fetch(fetchRequest).first {
                return result
                
            } else if shouldCreate {
                return Self(entity: Self.entity(), insertInto: sourceContext)
            }
            
        } catch {
            print("Fetch failed!! \(error.localizedDescription)")
        }
        
        return nil
    }
    
    public static func get(predicate: NSPredicate,
                           sortDescriptors: [NSSortDescriptor]? = nil,
                           sourceContext: NSManagedObjectContext = PersistenceController.instance.container.viewContext) -> [Self] {
        
        let fetchRequest = NSFetchRequest<Self>(entityName: "\(Self.self)")
        fetchRequest.predicate = predicate
        fetchRequest.sortDescriptors = sortDescriptors
        
        do {
            return try sourceContext.fetch(fetchRequest)

        } catch {
            print(error)
            return []
        }
    }
    
    public static func getAll(sourceContext: NSManagedObjectContext = PersistenceController.instance.container.viewContext) -> [Self] {
        let fetchRequest = NSFetchRequest<Self>(entityName: "\(Self.self)")
        fetchRequest.predicate = NSPredicate(format: "TRUEPREDICATE")
        do {
            return try sourceContext.fetch(fetchRequest)
            
        } catch {
            print("Fetch failed!! \(error.localizedDescription)")
        }
        
        return []
    }
}

extension NSManagedObjectContext: MapContext {}

extension Base where Self: NSManagedObject, Self: StaticMappable {

    public static func parse(json: JSONObject, complete: @escaping (Self) -> Void) {
        PersistenceController.instance.container.performBackgroundTask { (context) in
            parse(jsonArray: [json]) { objects in
                if !objects.isEmpty {
                    complete(objects[0])
                }
            }
        }
    }
    
    public static func parse(jsonArray: [JSONObject],
                             complete: @escaping ([Self]) -> Void) {
        let context = PersistenceController.instance.container.newBackgroundContext()
        
        context.perform {
            var entityIDs: [EntityID] = []
            ContextManager.instance.push(context: context,
            dataModify: {
                entityIDs = createAndFill(jsonArray: jsonArray, context: context)
                
            }, complete: {
                DispatchQueue.main.async {
                    let context: NSManagedObjectContext = context.parent ?? PersistenceController.instance.container.viewContext
                    let entites = Self.get(predicate: NSPredicate(format: "id IN %@", entityIDs), sourceContext: context)
                    complete(entites)
                }
            })
        }
    }
    
    static func createAndFill(jsonArray: [JSONObject],
                              context: NSManagedObjectContext) -> [EntityID] {

        let entityIDs = jsonArray.compactMap { $0[Self.jsonKey] as? EntityID }

        guard !entityIDs.isEmpty else {
            fatalError("Entity must have an unique ID in JSON dictionary")
        }
        _ = Mapper<Self>(context: context).mapArray(JSONArray: jsonArray)
        
        return entityIDs
    }
    
    public func delete(from context: NSManagedObjectContext = PersistenceController.instance.container.viewContext) {
        context.delete(self)
    }
    
    public static func deleteAll(from context: NSManagedObjectContext = PersistenceController.instance.container.viewContext) {
        delete(with: NSPredicate(format: "TRUEPREDICATE"), from: context)
    }
    
    public static func delete(with predicate: NSPredicate, from context: NSManagedObjectContext = PersistenceController.instance.container.viewContext) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "\(Self.self)")
        fetchRequest.predicate = NSPredicate(format: "TRUEPREDICATE")
        
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        batchDeleteRequest.resultType = .resultTypeCount

        do {
            let result = try context.execute(batchDeleteRequest) as! NSBatchDeleteResult
            print("The batch delete request has deleted \(result.result ?? 0) records.")
            context.reset()
            
        } catch {
            print("DELETE FAILED \(error)")
        }
    }
}

public extension Collection where Element: Base {
    func delete(from context: NSManagedObjectContext) {
        self.forEach { $0.delete(from: context) }
    }
}

extension Dictionary where Key == String, Value: Any {
    public func print() {
        let data: Data = try! JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
        Swift.print(String(data: data, encoding: .utf8)!)
    }
}

extension Array where Element == JSONObject {
    public func print() {
        self.forEach { $0.print() }
    }
}
