#
# Be sure to run `pod lib lint HDxPersistence.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'HDxPersistence'
  s.version          = '0.1.0'
  s.summary          = 'Pod uses Core data'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = 'Pod uses core data and object mapper'

  s.homepage         = 'https://MiloshBabic@bitbucket.org/MiloshBabic/hdxpersistence.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Milos Babic' => 'milos.babic@symphony.is' }
  s.source           = { :git => 'https://MiloshBabic@bitbucket.org/MiloshBabic/hdxpersistence.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'HDxPersistence/Classes/**/*'
  
  # s.resource_bundles = {
  #   'HDxPersistence' => ['HDxPersistence/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'ObjectMapper', '~> 3.3'
end
